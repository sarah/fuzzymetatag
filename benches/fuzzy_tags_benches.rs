use criterion::{criterion_group, criterion_main, BenchmarkId, Criterion};
use fuzzytags::FuzzyTagKeyPair;
use std::time::Duration;

fn benchmark_generate_tag(c: &mut Criterion) {
    let mut group = c.benchmark_group("generate_tags");
    group.measurement_time(Duration::new(10, 0));
    let key = FuzzyTagKeyPair::generate(24);
    for p in [5, 10, 15].iter() {
        group.bench_with_input(BenchmarkId::from_parameter(p), p, |b, _gamma| b.iter(|| key.public_key.generate_tag()));
    }
}

fn benchmark_test_tag(c: &mut Criterion) {
    let mut group = c.benchmark_group("test_tags");
    group.measurement_time(Duration::new(10, 0));
    let key = FuzzyTagKeyPair::generate(24);
    for p in [5, 10, 15].iter() {
        let tag = key.public_key.generate_tag();
        let detection_key = key.extract(*p);
        group.bench_with_input(BenchmarkId::from_parameter(p), p, |b, _gamma| b.iter(|| detection_key.test_tag(&tag)));
    }
}

criterion_group!(benches, benchmark_generate_tag, benchmark_test_tag);
criterion_main!(benches);
